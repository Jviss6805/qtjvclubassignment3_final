/**
 *  Document   : QTJVLoanServlet.java
 *  Created on : March 24, 2017, 4:55:39 PM
 *  Authors    : Quennie Teves & Jodi Visser
 */

package club.cart;

import club.business.Book;
import club.business.ECart;
import club.business.ELoan;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jodi
 */
public class QTJVClearCartServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        ArrayList<Book> loanList = (ArrayList<Book>) this.getServletContext().getAttribute("loanitems");
        ECart cart = new ECart();
        if(this.getServletContext().getAttribute("cart") == null) {
            this.getServletContext().setAttribute("cart", cart);
        } else {
            cart = (ECart) this.getServletContext().getAttribute("cart");
        }
                
        ArrayList<Book> cartBooks = cart.getItems();
        int count = cartBooks.size();
        
        for(int i = 0; i < count; i++) {
            Book cartBook = cartBooks.get(0);
            Book book = ELoan.findItem(loanList, cartBook.getCode());
            cart.removeItem(book);
            ELoan.addToQOH(loanList, cartBook.getCode(), cartBook.getQuantity());
        }
        
        HttpSession session = request.getSession();
        session.invalidate();
        
        getServletContext().getRequestDispatcher("/QTJVECart.jsp")
                .forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
