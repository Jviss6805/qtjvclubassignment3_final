/**
 *  Document   : QTJVLoanServlet.java
 *  Created on : March 20, 2017, 4:55:39 PM
 *  Authors    : Quennie Teves & Jodi Visser
 */

package club.cart;

import club.business.Book;
import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import club.business.ELoan;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
/**
 *
 * @author jvisser6805
 */
public class QTJVLoanServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ArrayList<Book> loanItems = new ArrayList<Book>();
               
        ServletContext application = getServletConfig().getServletContext();
        String file = this.getServletContext().getRealPath("/WEB-INF/books.txt");
        
        loanItems = ELoan.loadItems(file);
        
        if(this.getServletContext().getAttribute("loanitems") == null) {
            this.getServletContext().setAttribute("loanitems", loanItems);
        }
        
        getServletContext().getRequestDispatcher("/QTJVELoan.jsp")
                .forward(request, response);
        System.out.print(getInitParameter("booksFilePath"));
        //if(loadItems)
        ELoan.loadItems(file);
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
