/**
 *  Document   : QTJVAddBookServlet.java
 *  Created on : February 22, 2017, 4:55:39 PM
 *  Authors    : Quennie Teves & Jodi Visser
 */

package club.admin;

import club.business.Book;
import club.data.BookIO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class QTJVAddBookServlet extends HttpServlet {

    /**
     * Processes requests for both doPost and doGet
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        String code = (String) request.getParameter("code");
        String description = (String) request.getParameter("description");
        String quantity = request.getParameter("quantity");
        String errorMessage = "";

        if (code == null || code.isEmpty()) {
            errorMessage = "Book code is required<br>";
        }
        
        if (description == null || (description.isEmpty() || description.length() < 2)) {
            errorMessage += "Description is required and"
                    + " must have at least 2 characters<br>";
        }

        int qty = 0;
        if (quantity == null || quantity.isEmpty()) {
            errorMessage += "A quantity is required";
        } else {
            qty = Integer.valueOf((String) quantity);
            if (qty <= 0) {
                errorMessage += "Quantity must be a positive number<br>";
            }
        }
        
        if (errorMessage.isEmpty()) {
            Book book = new Book(code, description, qty);
            String path = this.getServletContext()
                    .getRealPath("/WEB-INF/books.txt");
            BookIO.insert(book, path);
            response.sendRedirect("QTJVDisplayBooks");
        } else {
            request.setAttribute("code", code);
            request.setAttribute("description", description);
            request.setAttribute("quantity", quantity);
            request.setAttribute("errorMessage", errorMessage);
            getServletContext().getRequestDispatcher("/QTJVAddBook.jsp")
                    .forward(request, response);
        }        
    }
    
    /**
     * Processes requests for doPost
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Processes requests for doGet
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }    
}
