/**
 *  Document   : QTJVDisplayBooksServlet.java
 *  Created on : February 15, 2017, 4:55:39 PM
 *  Authors    : Quennie Teves & Jodi Visser
 */

package club.admin;

import club.data.BookIO;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import club.business.Book;

public class QTJVDisplayBooksServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {

        String file = this.getServletContext().getRealPath("/WEB-INF/books.txt");
        ArrayList<Book> books = BookIO.getBooks(file);

        request.setAttribute("books", books);
        getServletContext().getRequestDispatcher("/QTJVDisplayBooks.jsp")
                .forward(request, response);
    }

    /**
     * Processes requests for doGet method
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
