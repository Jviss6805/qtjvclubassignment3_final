<%-- 
    Document   : QTJVELoan
    Created on : Mar 24, 2017, 3:17:18 PM
    Authors    : Quennie Teves & Jodi Visser
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="QTJVBanner.jsp" />
<section>
    <c:set var="total" value="${0}"/>
    <p class="center"><strong>Your Loan Cart</strong></p>
    <table>
        <tr>
            <th>Code</th>
            <th>Description</th>
            <th>quantity</th>
        </tr>
        <c:forEach var="book" items="${applicationScope.cart.getItems()}">
        <tr>
            <td><c:out value="${book.code}" /></td>
            <td><c:out value="${book.description}" /></td>
            <td class="pull-right"><c:out value="${book.quantity}" /></td>            
        </tr>
        <c:set var="total" value="${total + book.quantity}" />
        </c:forEach>
        <tr><br>
            <td colspan="3" class="pull-right">Total: <c:out value="${total}" /></td><br>
        </tr>
    </table>
        <section>
            <a href="QTJVClearCart">Clear the cart</a><br>
            <a href="QTJVLoan">Return to eLoan</a>
        </section>
</section>
<jsp:include page="QTJVFooter.jsp" />