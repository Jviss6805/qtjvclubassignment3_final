<%-- 
    Document   : QTJVBanner
    Created on : Jan 25, 2017, 4:28:17 PM
    Authors    : Quennie Teves & Jodi Visser
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link href="styles/main.css" type="text/css" rel="stylesheet">
        <link href="styles/qtjv.css" type="text/css" rel="stylesheet">
        <title>QTJV Computer Programming Club</title>
    </head>
    <body>
        <header>
            <img src="images/qtjv-icon.png">
            <h1>QTJV Computer Programming Club</h1>
            <h2>IT@Conestoga</h2>
        </header>

        <nav id="nav_bar">
            <ul>
                <li><a href="QTJVIndex.jsp">Home</a></li>
                <li><a href="QTJVRegister.jsp">Register</a></li>
                <li><a href="QTJVLoan">eLoan</a></li>
                <li><a href="QTJVCart">eCart</a></li>
                <li><a href="QTJVAdmin.jsp">Admin</a></li>
            </ul>
        </nav>
