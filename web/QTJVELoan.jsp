<%-- 
    Document   : QTJVELoan
    Created on : Mar 24, 2017, 3:17:18 PM
    Authors    : Quennie Teves & Jodi Visser
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="QTJVBanner.jsp" />
<section>
    <table>
        <tr>
            <th>Code</th>
            <th>Description</th>
            <th>QOH</th>
            <th>Action</th>
        </tr>
        <c:forEach var="book" items="${applicationScope.loanitems}">
        <tr>
            <td><c:out value="${book.code}" /></td>
            <td><c:out value="${book.description}" /></td>
            <td class="pull-right"><c:out value="${book.quantity}" /></td>
            <td>
                <c:if test="${book.quantity > 0}">
                    <a href="QTJVCart?action=reserve&code=${book.code}">Reserve</a>
                </c:if>
                <c:if test="${book.quantity == 0}">N/A</c:if>
            </td>
            
        </tr>       
        </c:forEach>
    </table>
</section>
<jsp:include page="QTJVFooter.jsp" />