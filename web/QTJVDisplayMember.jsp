<%-- 
    Document   : QTJVDisplayMember
    Created on : Jan 25, 2017, 5:26:17 PM
    Authors    : Quennie Teves & Jodi Visser
--%>

<jsp:include page="QTJVBanner.jsp" />        
<section>
    <h2>Thanks for joining our club!</h2>
    <br>
    <p>Here is the information you entered:</p>
    <label for="fullname">Full Name:</label>
    ${param.fullname}<br>
    <label for="email">Email:</label>
    ${param.email}<br>
    <label for="phone">Phone:</label>
    ${param.phone}<br>
    <label>IT Program:</label>
    ${param.program}<br>
    <label>Year Level:</label>
    ${param.level}<br>

    <p>To register another member, click on the Back button in 
        your browser or the Return button shown below.</p>
    
    <form action='QTJVRegister.jsp' method='get'>
        <input type='submit' value='Return'>
    </form>
</section>
<jsp:include page="QTJVFooter.jsp" />
